import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { BaseUrlLogin, ContentType } from '../ConfigURL/ConfigURL';
import axios from 'axios';
import { Button, Form, Input } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

export default function LoginPage() {
    const history = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const res = await axios.post(
                    BaseUrlLogin,
                    { username, password },
                    {
                        headers: {
                            'Content-Type': ContentType,
                        },
                    }
                );
                const token = res.data.data.token;
                localStorage.setItem('token', token);
                if (token) {
                    history('/HomePage');
                } else {
                    history('/');
                }
            } catch (err) {
                console.log('err: ', err);
            }
        };

        fetchData();
    }, [username, password]);

    const onFinish = async (values) => {
        setUsername(values.username);
        setPassword(values.password);
    };

    return (
        <div className="flex items-center justify-center h-screen">
            <div className="w-full max-w-xs">
                <div className="text-center mb-6">
                    <img src="fishsh.png" alt="" className="w-24 mx-auto" />
                </div>
                <div className="bg-white rounded shadow px-8 pt-6 pb-8 mb-4">
                    <p className="text-2xl font-bold text-center mb-4">Hệ Thống Quản Lý Ao</p>
                    <p className="text-center text-gray-500 text-sm mb-6">Đăng nhập để tiếp tục</p>
                    <Form name="normal_login" className="login-form" onFinish={onFinish}>
                        <Form.Item
                            name="username"
                            rules={[{ required: true, message: 'Vui lòng nhập tên đăng nhập!' }]}
                        >
                            <Input
                                className="mb-4"
                                prefix={<UserOutlined className="text-xl" />}
                                placeholder="Tên đăng nhập"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Vui lòng nhập mật khẩu!' }]}
                        >
                            <Input.Password
                                className="mb-4"
                                prefix={<LockOutlined className="text-xl" />}
                                type="password"
                                placeholder="Mật khẩu"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="w-full">
                                Đăng nhập
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
