import axios from "axios";
export const BaseUrlLogin = "http://easyfeed.vn/v1/login"; // API login
export const ContentType = "application/json; charset=utf-8"

export const listFarmingArea = '/farmingareas'; //Api load vùng nuôi
export const startAndlimit = { // params của API load vùng nuôi
    "start": 0, "limit": 0
}


export const loadFishPond = "/getfishpond"; // API danh sách ao
export const loadFishPondData = { //Params của API danh sách ao
    "data": {
    }
}
export const getSumByDay = "/getsummassbydate"

export const ApiFeedByDate = "/getfeedbydate"; // API load thức ăn theo kg trên ngày
export const ApiUpdateFeed = "/updatefeed"


export const https = axios.create({ //https của tất cả API 
    baseURL: "http://easyfeed.vn/v1",
})
https.interceptors.request.use( // quét token liên tục
    (config) => {
        const token = localStorage.getItem("token");
        if (token) {
            config.headers["token"] = token;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);
