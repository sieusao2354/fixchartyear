import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LoginPage from './Login/LoginPage';
import ChartYear from './Chart/ChartYear';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<LoginPage />} />
          <Route path='/HomePage' element={<ChartYear />} />

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
